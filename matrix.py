#!/usr/bin/env python

from pyA20.gpio import  gpio
from pyA20.gpio import port 

gpio.init()

MATRIX = [ [1,2,3,'A'],
	   [4,5,6,'B'],
	   [7,8,9,'C'],
	   ['*',0,'#','D'] ]

ROW = [port.PA7,port.PA11,port.PA13,port.PA21]
COL = [port.PA12,port.PA14,port.PA18,port.PA20]

for j in range(4) :
	gpio.setcfg(COL[j], gpio.OUTPUT)
	gpio.output(COL[j], 1)

for i in range (4):
	gpio.setcfg(ROW[i], gpio.INPUT)
	gpio.pullup(ROW[i], gpio.PULLUP)

try:
	while (True):
		for j in range (4):
			gpio.output(COL[j],0)
			
			for i in range (4):
				if gpio.input(ROW[i]) == 0:
					print MATRIX [i] [j]
		
			gpio.output(COL[j], 1)
	

except KeyboardInterrupt:
	print ("Goodbye")

	
